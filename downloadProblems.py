#!/usr/bin/env python3

"""downloadProblems.py: Basic script that downloads all block worlds problem files from 
                        http://www.cs.colostate.edu/meps/repository/aips2000.html
   Syntax:
   python3 downloadProblems.py"""


__pythonversion__ = "3.6.0"
__author__ = "Oikonomou Dimitrios"
__studentID__ = "dai16260"
__email__ = "dai16260@uom.edu.gr"
__version = "1.0"

import re
import requests

def main():

    siteurl = "http://www.cs.colostate.edu/meps/repository/aips2000.html"

    pagecontent = requests.get(siteurl).text

    pattern = r"http://www.cs.colostate.edu/meps/aips2000data/2000-tests/blocks/probBLOCKS-\d+-\d+.pddl"

    for url in re.findall(pattern, pagecontent):

        print("Downloading: " + url)
        problem = requests.get(url).text

        with open("problems/" + url.split("/")[-1], "w") as file:

            file.write(problem)



if __name__ == "__main__":
    
    main()
