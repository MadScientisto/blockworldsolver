#!/usr/bin/env python3

"""generateStats.py: Tries to solve all .pddl problems in the problems folder in 60 seconds,
                    writes solutions in solutions folder and
                    compiles results in stats.csv
   Syntax:
   python3 generateStats"""


__pythonversion__ = "3.6.0"
__author__ = "Oikonomou Dimitrios"
__studentID__ = "dai16260"
__email__ = "dai16260@uom.edu.gr"
__version = "1.0"

import bw
import os, csv

def main():

    timelimit = 60
    algorithms = ["depth", "breadth", "best", "astar"]

    with open("stats.csv", mode='w') as statfile:
        
        writer = csv.writer(statfile, delimiter=',')
        header =(["filename", 
                  "depthTime", "depthSteps", "depthTreesize",
                  "breadthTime", "breadthSteps", "breadthTreesize",
                  "bestTime", "bestSteps", "bestTreesize",
                  "astarTime", "astarSteps", "astarTreesize"])
        writer.writerow(header)

        for f in sorted(os.listdir("problems")):
            if f.endswith(".pddl"):

                row =[f]
                for alg in algorithms:

                    solutionfilepath = "solutions/{}{}.txt".format(alg,f[:-5])

                    problem = bw.BlockWorldProblem("problems/"+f)
                    res = problem.solve(alg, solutionfilepath=solutionfilepath, timelimit=timelimit, screentimer=True)
                    
                    row.extend(["{0:.2f}".format(res.time), res.movecount, res.treesize])

                writer.writerow(row)




if __name__ == "__main__":
    
    main()