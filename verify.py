#!/usr/bin/env python3

"""verify.py: Checks if solution is correct
   Syntax:
   python3 verify.py <problem> <solution>

   problem: filepath of .pddl problem file
   solution: filepath where solution is written"""


__pythonversion__ = "3.6.0"
__author__ = "Oikonomou Dimitrios"
__studentID__ = "dai16260"
__email__ = "dai16260@uom.edu.gr"
__version = "1.0"

import bw
import sys, re

class solutionChecker(bw.BlockWorldProblem):
    """Reads problem and solution files and checks if solution is correct.
       Inherits from BlockWorldProblem to use the already implemented parser function"""

    def __init__(self, problemfilepath, solutionfilepath):
        
        super().__init__(problemfilepath)
        self.solutionfilepath = solutionfilepath
        self.moves = self.parseSolution()
    
    def parseSolution(self):
        """Reads solution and returns list of Move objects"""

        try:
            file = open(self.solutionfilepath, "r")
        except Exception as E:
            print("Could not open {}".format(self.solutionfilepath))
            exit(1)

        solution = file.read()
        file.close()

        moves = []
        for move in solution.splitlines():

            #Isolate arguments
            match = re.match(r'.*Move\((.*),\s(.*),\s(.*)\)',move)
            if match: 
                arguments = match.groups()
            else: 
                print("Line: {} has not valid format".format(move))
                exit(1)

            #Create move and append it to moves
            moves.append(bw.Move(arguments[0], arguments[1], arguments[2]))
        
        return moves

    def isCorrect(self):
        """Returns True if solution is correct and message  """

        #Iterate over moves
        for i, move in enumerate(self.moves):
            
            moved = None
            origin = None
            destination = None

            #Select appropriate blocks
            for j in range(len(self.root.blocks)):

                if self.root.blocks[j].name == move.block: moved = self.root.blocks[j]
                if self.root.blocks[j].name == move.origin: origin = self.root.blocks[j]
                if self.root.blocks[j].name == move.final: destination = self.root.blocks[j]
            
            #Check if block names are valid
            if moved == None: 
                return False, "Block {} does not exist".format(move.block), (i, move)
            if origin == None and not move.origin=="table": 
                return False, "Block {} does not exist".format(move.origin), (i, move)
            if destination == None and not move.final=="table": 
                return False, "Block {} does not exist".format(move.final), (i, move)

            #Check if block is movable 
            if moved.top != "clear": 
                return False, "Block {} cannot be moved".format(moved.name), (i, move)
            #Check if destination is clear
            if destination:
                if destination.top != "clear" and destination.name != moved.bottom: 
                    return False, "Block {} cannot move to {}".format(moved.name, destination.name), (i, move)
            #Check if move origin is the same with bottom of moved block
            if origin:
                if origin.top != moved.name:
                    return False, "Block {} is not on top of {}".format(moved.name, origin.name), (i, move)
            elif moved.bottom!="table":
                    return False, "Block {} is not on table".format(moved.name), (i, move)

            
            #Update Nodes
            if destination: 
                moved.bottom = destination.name
                destination.top = moved.name
            else: 
                moved.bottom = "table"

            if origin and not origin==destination: origin.top = "clear"


        #Check if root is now equal to goal 
        if self.root.compare(self.goalNode): return True, "Solution is Correct", None
        else: return False, "Solution is not Correct", (i, move)



def main():

    if len(sys.argv) < 3 or len(sys.argv) > 3:
        print("Wrong number of arguments.\nSyntax: python3 verify.py <problem> <solution>")

    problemfilepath = sys.argv[1]
    solutionfilepath = sys.argv[2]

    checker = solutionChecker(problemfilepath, solutionfilepath)

    correct, msg, move = checker.isCorrect()

    if not correct:
        print("{}. Move:({}, {}, {})".format(move[0]+1, move[1].block, move[1].origin, move[1].final))
    print(msg)


if __name__ == "__main__":
    
    main()