#!/usr/bin/env python3

"""bw.py: Reads block worlds puzzles, solves them and writes solution to specified txt file
   Syntax:
   python3 bw.py <algorithm> <problem> <solution> (timelimit)

   algorithm: algorithm to be used for the solution: depth, breadth, best, astar
   problem: filepath to the .pddl problem file
   solution: filepath where solution is to be written
   timelimit: optional argument specifies maximum time to solve the problem"""


__pythonversion__ = "3.6.0"
__author__ = "Oikonomou Dimitrios"
__studentID__ = "dai16260"
__email__ = "dai16260@uom.edu.gr"
__version = "2.0"

import re, copy, sys, time, operator

#Increase recursion limit because of big tree sizes
sys.setrecursionlimit(10000)

class Block:

    def __init__(self, name, top, bottom):

        self.name = name      #Name of the block
        self.top = top        #Name of the block on top e.g. "A","B","clear"
        self.bottom = bottom  #Name of the block on the bottom e.g. "A", "B", "table"

    def __eq__(self, otherBlock):
        """Compares two blocks and returns True if they are equal"""
        
        if otherBlock==None: return False
        return self.name == otherBlock.name and self.top == otherBlock.top and self.bottom==otherBlock.bottom

    def __ne__(self, otherBlock):
        """Opposite of __eq__"""

        return not self.__eq__(otherBlock)

class Move:

    def __init__(self, block, origin, final):

        self.block = block   #Name of the moved block e.g. A, B or C
        self.origin = origin #Name of the starting position of the moved block e.g. table, A, B, C
        self.final = final   #Name of the final position of the moved block e.g. table, A, B, C


class TreeNode:

    def __init__(self, blocks, lastMove=None, parent=None):
        
        self.children = []                #List of the all the possible children of the tree node
        self.blocks = blocks              #List of the blocks
        self.parent = parent              #Parent node
        self.lastMove = lastMove          #Object of class Move representing the move from the parent node
        if parent: self.depth = parent.depth + 1
        else: self.depth = 0              #Depth of the node, 0 if it is the root
        self.h = 0                        #Heurestic value for best first algorithm
        self.a = 0                        #Value for the A* algorithm
        

    def heuristic(self, goalNode):
        """Calculates the minimum amount of moves between this Tree Node and the goal
            If the block is not in correct position it needs at least one move
            If the top block is in correct position it means that it needs to 
            move with it's bottom"""

        wrongBlocks = 0
        for block in self.blocks:
            for goalblock in goalNode.blocks:
                #Find matching blocks
                if block.name == goalblock.name:
                    #Check if in correct position
                    if block.bottom != goalblock.bottom: 
                        wrongBlocks+=1
                        #Check if top is correct
                        if block.top == goalblock.top: wrongBlocks+=1

        self.h = wrongBlocks
        self.a = self.h + self.depth

    def compare(self, otherNode):
        """Compares two Tree Node block lists and returns True if they are equal"""

        for block in self.blocks:
            if block not in otherNode.blocks:
                return False

        for block in otherNode.blocks:
            if block not in self.blocks:
                return False
        return True 

    def compileMoves(self):
        """Returns a list with all the previous moves"""

        if not self.parent:
            return []
        else:
            return self.parent.compileMoves() + [self.lastMove]



    def isInTree(self, newNode):
        """Checks if a Node is a descendant of the Node
           Input: Node to be tested
           Output: Boolean"""

        if self.compare(newNode):
            return True
        else:
            for child in self.children:
                if child.isInTree(newNode): 
                    return True
        return False

    def hasAncestor(self, newNode):
        """Checks if a Node is an ancestor of this node
           (slightly faster than isInTree)
           Input: Node to be tested
           Output: Boolean"""
        
        if self.compare(newNode):
            return True
        elif self.parent:
            return self.parent.hasAncestor(newNode)
        return False

class Result:
    """Class of objects returned by BlockWorldProblem.solve()"""

    def __init__(self, problemfilepath, solutionfilepath, algorithm):
        
        self.problemfilepath = problemfilepath
        self.solutionfilepath = solutionfilepath
        self.algorithm = algorithm
        self.solutionFound = False
        self.time = -1
        self.movecount = -1
        self.treesize = -1

    def __repr__(self):
        """Method used when print() is called on the object"""

        if self.solutionFound:
            return "The {0} problem was solved in {1:.2f} seconds and the solution was written in {2}".format(self.problemfilepath, self.time, self.solutionfilepath)
        else:
            return "Could not solve the problem withing specified timelimit"


class BlockWorldProblem:

    def __init__(self, problemfilepath):

        self.problemfilepath = problemfilepath
        self.blocks = []         #Helper list of all the blocks: on table and clear 
        self.init = []           #List of the initial blocks
        self.goal = []           #List of the final blocks
        self.frontier = []       #List of the frontier nodes
        self.treesize = 1        #Count of the nodes in the tree

        self.init, self.goal = self.parser()
        self.root = TreeNode(self.init, None)      #Root node of the tree
        self.goalNode = TreeNode(self.goal, None)  #Goal node to compare with potential solutions(not in the tree)
        self.frontier.append(self.root)

    def parser(self):
        """Reads the problem from the specified filepath
        and returns the appropriate blocks, init and goal lists"""

        def buildBlockList(states):
            """Builds either init or final block lists"""

            #Make a copy of the helper list
            blockList = copy.deepcopy(self.blocks)
            #Iterate over states
            for state in re.findall(r'\(.*?\)', states, re.DOTALL):
                if state.split()[0] == "(ON":

                    #Tokenize arguments
                    top = state.split()[1]          #first argument
                    bottom = state.split()[2][:-1]  #second argument

                    #Iterate over blocks and update values
                    for i in range(len(blockList)):
                        if blockList[i].name == top: blockList[i].bottom = bottom
                        elif blockList[i].name == bottom: blockList[i].top = top

            return blockList

        try:
            file = open(self.problemfilepath, "r")
        except:
            print("Could not open {}".format(self.problemfilepath))
            exit(1)

        text = file.read()
        file.close()

        #Isolate required lines
        try:
            objects = re.findall(r'\(:objects.*?\)', text, re.IGNORECASE | re.DOTALL)[0][9:]
            initstates = re.findall(r'\(:INIT.*?\)\)', text, re.IGNORECASE | re.DOTALL)[0][7:-1]
            goalstates = re.findall(r'\(:goal.*?\)\)\)', text, re.IGNORECASE | re.DOTALL)[0][12:-2]
        except:
            print("Not valid problem format!")
            exit(1)

        #Create helper list
        self.blocks = [Block(blockname,"clear","table") for blockname in objects.split()[:-1]]
        
        #Build block lists
        init = buildBlockList(initstates)
        goal = buildBlockList(goalstates)
        

        return init, goal

    def makeChildren(self, currentNode, heurestic=False):
        """Creates all the possible children of the node, and calculates their heurestic values if required"""

        for movedBlock in currentNode.blocks:
            #Check if block can be moved
            if movedBlock.top == "clear":
                """Iterate over all destinations except table"""
                for destinationBlock in currentNode.blocks:
                    #Check if destination is valid
                    if destinationBlock.top == "clear" and not movedBlock==destinationBlock:
                        #Make a copy the blocks
                        tempBlocks = copy.deepcopy(currentNode.blocks)

                        #Make the move
                        tempMove = Move(movedBlock.name, movedBlock.bottom, destinationBlock.name)
                        for i in range(len(tempBlocks)):
                            #Update the necessary blocks
                            if tempBlocks[i].name == movedBlock.name:
                                tempBlocks[i].bottom = destinationBlock.name
                            elif tempBlocks[i].name == destinationBlock.name:
                                tempBlocks[i].top = movedBlock.name
                            elif tempBlocks[i].name == movedBlock.bottom:
                                tempBlocks[i].top = "clear"

                        #Create child
                        tempChild = TreeNode(tempBlocks, tempMove, parent=currentNode)
                        if heurestic: tempChild.heuristic(self.goalNode)
                        #Check if it is in Tree and append it
                        #if not self.root.isInTree(tempChild): currentNode.children.append(tempChild)
                        #Check if it is ancestor of parent and append it
                        if not currentNode.hasAncestor(tempChild): currentNode.children.append(tempChild)

                """Make move to table"""
                tempMove = Move(movedBlock.name, movedBlock.bottom, "table")
                tempBlocks = copy.deepcopy(currentNode.blocks)
                for i in range(len(tempBlocks)):
                    #Update the necessary blocks
                    if tempBlocks[i].name == movedBlock.name:
                                tempBlocks[i].bottom = "table"
                    elif tempBlocks[i].name == movedBlock.bottom:
                                tempBlocks[i].top = "clear"
                    #Create child
                tempChild = TreeNode(tempBlocks, tempMove, parent=currentNode)
                if heurestic: tempChild.heuristic(self.goalNode)
                #Check if it is in Tree and append it
                #if not self.root.isInTree(tempChild): currentNode.children.append(tempChild)
                #Check if it is ancestor of parent and append it
                if not currentNode.hasAncestor(tempChild): currentNode.children.append(tempChild)


    def isSolution(self, potentialSolution):
        """Checks whether a Tree Node is a solution"""
        return self.goalNode.compare(potentialSolution)
    
    def writeSolution(self, solution, solutionFile):
        """Writes the solution to the specified file object"""
        
        moves = solution.compileMoves()
        for i, move in enumerate(moves):
            try:
                solutionFile.write("{}. Move({}, {}, {})\n".format(i+1, move.block, move.origin, move.final))
            except Exception as E:
                raise E
    
    def addToFrontier(self, parent):
        """Adds the children of the parent node to the frontier if the are not already in the tree"""

        self.frontier.extend(parent.children)

    def depth(self):
        """Returns the last node added to the frontier and removes it from the frontier"""

        return self.frontier.pop()

    def breadth(self):
        """Returns the first node added to the frontier and removes it from the frontier"""

        return self.frontier.pop(0)

    def astar(self):
        """Sorts frontier list by the a value and returns the node with the lowest"""
        self.frontier.sort(key=operator.attrgetter("a"))
        
        return self.frontier.pop(0)

    def best(self):
        """Sorts frontier list by the h value and returns the node with the lowest"""
        self.frontier.sort(key=operator.attrgetter("h"))

        return self.frontier.pop(0)

    def solve(self, selectedAlg, solutionfilepath="solution.txt", timelimit=None, screentimer=False):
        """Solves the problem
           Input: selectedAlg: Algorithm selected by user e.g. "depth" or "breadth" or "astar" or "best", 
                  solutionfilepath: filepath where solution is written,
                  timelimit: maximum time to solve the problem, no timelimit if not specified
           Output: solution to the problem if it exists"""

        #Check input for errors
        algs = {"depth": self.depth, "breadth": self.breadth, "astar": self.astar, "best": self.best}
        if selectedAlg not in algs:
            print("Specified method: {} is not supported".format(selectedAlg))
            return
        
        try:
            solutionFile = open(solutionfilepath, 'w')
        except Exception as E:
            raise E

        #Create Result object
        result = Result(self.problemfilepath, solutionfilepath, selectedAlg)

        #Print start message
        print("Solving {}: with {}".format(self.problemfilepath, selectedAlg))

        #Initiate timer
        startTime = time.time()
        currentTime = startTime

        solutionFound = False
        while (not solutionFound) and (timelimit==None or currentTime-startTime<=timelimit):

            #Increment timer
            currentTime = time.time()

            #Print timer if required by user
            if screentimer:
                sys.stdout.write("\r")
                sys.stdout.write("Elapsed time: {0:.2f}s".format(currentTime-startTime))
                sys.stdout.flush()

            #Select a frontier node and make its children
            currentNode = algs[selectedAlg]()
            self.makeChildren(currentNode, heurestic=(selectedAlg=="astar" or selectedAlg=="best"))
            
            #increment tree size
            self.treesize+=len(currentNode.children)

            #Check if any of the children are a solution
            for child in currentNode.children:
                if self.isSolution(child):

                    result.solutionFound = True
                    result.time = currentTime-startTime
                    result.movecount = len(child.compileMoves())

                    self.writeSolution(child, solutionFile)
                    
                    solutionFound = True

            #Add the children to the frontier and increment treesize
            self.addToFrontier(currentNode)

        #Store treesize    
        result.treesize=self.treesize

        print("\r")
        solutionFile.close()
        return result

            

def main():

    if len(sys.argv) < 4 or len(sys.argv) > 5:
        print("Wrong number of arguments.\nSyntax: python3 bw.py <algorithm> <problem> <solution> (timelimit)")

    selectedAlg = sys.argv[1]
    problemfilepath = sys.argv[2]
    solutionfilepath = sys.argv[3]

    if len(sys.argv) == 5:
        try:
            timelimit=int(sys.argv[4])
        except:
            print("Not valid timelimit")
            exit(1)
    else:
        timelimit=None
            
    
    problem = BlockWorldProblem(problemfilepath)
    res = problem.solve(selectedAlg, solutionfilepath=solutionfilepath, timelimit=timelimit, screentimer=True)
    print(res)

if __name__ == "__main__":
    
    main()