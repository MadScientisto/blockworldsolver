                        **BlockWorld**
---------------------------------------------------------

**Description**:

My implementation of basic AI search algorithms intended for educational purposes:
Solves Block World problems from 
http://www.cs.colostate.edu/meps/aips2000data/2000-tests/blocks/probBLOCKS-4-2.pddl
using depth, breadth, best-first and A* search algorithms.

---------------------------------------------------------

**Prerequisites**:

1. Python 3.6.0 or above
2. requests external library (pip install requests)

---------------------------------------------------------

**Script**s:

bw.py:

    Syntax: python3 bw.py <algorithm> <problem> <solution> (timelimit)

            algorithm: algorithm to be used for the solution: depth, breadth, best, astar
            problem: filepath to the .pddl problem file
            solution: filepath where solution is to be written
            timelimit: optional argument specifies maximum time to solve the problem

    Description: 
        Reads block worlds puzzles, solves them and writes solution to specified txt file

verify.py:

    Syntax: python3 verify.py <problem> <solution>

            problem: filepath of .pddl problem file
            solution: filepath where solution is written

    Description: 
        Checks if solution is correct

generateStats.py:

    Syntax: python3 generateStats

    Description: 
        Tries to solve all .pddl problems in the problems folder in 60 seconds,
        writes solutions in solutions folder and compiles results in stats.csv

downloadProblems.py:

    Syntax:
        python3 downloadProblems.py

    Description:
        Basic script that downloads all block worlds problem files from 
        http://www.cs.colostate.edu/meps/repository/aips2000.html


---------------------------------------------------------

**Directories and Files**:

problems/:

        Directory where downloadProblems.py saves downloaded problems and
        generateStats.py looks for problems to solve.

solutions/:

        Directory where generateStats.py saves solutions.


